//
//  PhotosVideosViewController.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 15/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit

class PhotosVideosViewController: UIViewController {

    @IBOutlet weak var tableView: PhotosVideosTableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var vwCategories: ScrollableMenu!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.tableView.delegate = self
        vwCategories.addObserver { (notification) in
            print("Notification selected category: \(notification)")
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
    }

    @IBAction func clickedBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
       // self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}
extension PhotosVideosViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "detail", sender: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116
    }
}

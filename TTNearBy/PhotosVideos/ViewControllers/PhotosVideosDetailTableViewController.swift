//
//  PhotosVideosDetailTableViewController.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 15/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit

class PhotosVideosDetailTableViewController: UITableViewController {

    @IBOutlet weak var vwImages: ScrollableImages!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPage: UILabel!
    
    var originalDescription: NSAttributedString!
    var shortDescription: NSAttributedString?
    
    var isDescriptionExpanded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100
        
        vwImages.images = ["image", "balloons", "pens", "stars"]
        vwImages.label = lblPage
        
        lblDescription.text = "Test pa more in the world of pa more. Isa laban sa lahat okay lang po ba? Ako na ang nanalo kaya uwi na kayo. At ikaw pa rin ang panalo, ikaw at wala ng iba. Edi ikaw na. Wot wot. Babalo"
        originalDescription = lblDescription.attributedText

        if lblDescription.calculateMaxLines() > 3 {
            let readmoreFont = UIFont.systemFont(ofSize: 16)
            let readmoreColor = UIColor.green
            lblDescription.addTrailing(with: "... ", moreText: "Read more", moreTextFont: readmoreFont, moreTextColor: readmoreColor)
            shortDescription = lblDescription.attributedText
        }
    }
    @IBAction func clickedBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            if shortDescription != nil {
                isDescriptionExpanded = !isDescriptionExpanded
                
                if isDescriptionExpanded {
                    lblDescription.attributedText = originalDescription
                }
                else {
                    lblDescription.attributedText = shortDescription
                }
            }
            //tableView.reloadData()
            tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    //MARK: Scrollview delegates
}

//
//  PhotosVideosRelatedCollectionView.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 15/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit

class PhotosVideosRelatedCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let num = 4
    let numOfRows: CGFloat = 2.5
    let space: CGFloat = 4
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.register(UINib.init(nibName: "PhotosVideosRelatedCollectionViewCell", bundle: Bundle(for: type(of: self))), forCellWithReuseIdentifier: "cell")
        self.delegate = self
        self.dataSource = self
        
        if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout   {
            layout.scrollDirection = .horizontal
        }
        self.isDirectionalLockEnabled = true
        self.isPagingEnabled = true
        self.backgroundColor = UIColor.white
    }
    func setup() {
        self.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return num
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotosVideosRelatedCollectionViewCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: space * 2, left: space, bottom: space * 2, right: space)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.frame.width / numOfRows) - (numOfRows * space)
        return CGSize(width: width, height: self.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return space
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return space
    }
}

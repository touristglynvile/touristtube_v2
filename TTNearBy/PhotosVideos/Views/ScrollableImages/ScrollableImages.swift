//
//  ScrollableImages.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 16/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit

class ScrollableImages: UIView, UIScrollViewDelegate {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    public var label: UILabel? {
        didSet {
            if let images = images {
                self.label?.text = " 1/\(images.count)"
            }
        }
    }
    
    public var images: Array<String>! {
        didSet {
            for i in 0..<images.count {
                let image = images[i]
                let imgView = UIImageView(image: UIImage(named: image, in: Bundle(for: type(of: self)), compatibleWith: nil))
                let pos = CGFloat(i) * self.frame.width
                imgView.frame = CGRect(x: pos, y: 0, width: self.frame.width, height: self.frame.height)
                imgView.contentMode = .scaleAspectFill
                imgView.clipsToBounds = true
                self.scrollView.addSubview(imgView)
            }
            self.scrollView.contentSize = CGSize(width: self.frame.width * CGFloat(images.count), height: 1)
            self.label?.text = " 1/\(images.count)"
        }
    }
    private var scrollView: UIScrollView!
    
    private func commonInit() {
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        scrollView.isPagingEnabled = true
        scrollView.isDirectionalLockEnabled = true
        scrollView.backgroundColor = UIColor.clear
        scrollView.delegate = self
        
        self.backgroundColor = UIColor.white
        self.addSubview(scrollView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexOfPage = Int((scrollView.contentOffset.x+(0.5*scrollView.frame.size.width))/scrollView.frame.width)+1
        label?.text = " \(indexOfPage)/\(images.count)"
        
    }
}

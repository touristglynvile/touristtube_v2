//
//  PhotosVideosTableViewCell.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 14/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit

class PhotosVideosTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    
    func setupUI() {
        imgView.layer.cornerRadius = 10
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  PhotosVideos.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 15/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit

class PhotosVideosTableView: UITableView, UITableViewDelegate, UITableViewDataSource {
    
    let num = 10
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.register(UINib(nibName: "PhotosVideosTableViewCell", bundle: Bundle(for: PhotosVideosViewController.self)), forCellReuseIdentifier: "cell")
        //self.delegate = self
        self.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return num
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhotosVideosTableViewCell
        
        return cell
    }
}

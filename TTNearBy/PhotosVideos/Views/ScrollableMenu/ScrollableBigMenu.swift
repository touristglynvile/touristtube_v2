//
//  ScrollableBigMenu.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 15/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit

class ScrollableBigMenu: ScrollableMenu {

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override var font: UIFont {
        return UIFont.systemFont(ofSize: 16)
    }
    
    override var button: UIButton {
        let btn = UIButton(type: .system)
        let font = self.font
        btn.titleLabel?.font = font
        btn.backgroundColor = UIColor.darkGray
       // btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.layer.borderWidth = 1
        btn.layer.cornerRadius = 10
        btn.setTitleColor(UIColor.white, for: .normal)
        return btn
    }
    
    
}

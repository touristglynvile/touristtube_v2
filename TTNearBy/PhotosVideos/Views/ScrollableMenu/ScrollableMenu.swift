//
//  ScrollableMenu.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 15/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit

class ScrollableMenu: UIView {

    // string separated by comma
    @IBInspectable var list: String? {
        didSet {
            print("list found: \(String(describing: list))")
            self.setupButtons()
        }
    }
    var items: Array<String> {
        return list?.components(separatedBy: ",") ?? []
    }
    private var buttons = Array<UIButton>()
    
    var scrollView: UIScrollView!
    private var observer: NSObjectProtocol?

    var font: UIFont {
        return UIFont.systemFont(ofSize: 12)
    }
    var topMargin: CGFloat {
        return 10
    }
    var space: CGFloat {
        return 8
    }
    var margin: CGFloat {
        return 16
    }
    
    var widths: CGFloat = 20
    
    var button: UIButton {
        let btn = UIButton(type: .system)
        let font = self.font
        btn.titleLabel?.font = font
        btn.backgroundColor = UIColor.white
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.layer.borderWidth = 1
        btn.layer.cornerRadius = 10
        btn.setTitleColor(UIColor.black, for: .normal)
        return btn
    }
    
    func setupButtons() {
        
        for i in 0..<items.count {
            let item = items[i]
            
            let btn = self.button
            let attr = [NSAttributedString.Key.font: self.font]
            let width = (item as NSString).size(withAttributes: attr).width + self.margin
            btn.frame = CGRect(x: widths, y: topMargin / 2, width: width, height: self.frame.height - self.topMargin)
            
            btn.setTitle(item, for: .normal)
            widths += (width + space)
            self.addTarget(to: btn)
            self.buttons.append(btn)
            scrollView.addSubview(btn)
        }
        
        scrollView.contentSize = CGSize(width: widths, height: 1)
    }
    deinit {
        self.removeObserver()
    }
    func addTarget(to button: UIButton) {
        button.addTarget(self, action: #selector(clickedButton(sender:)), for: .touchUpInside)
    }
    func addObserver(with block: @escaping (Notification) -> Void) {
        observer = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "selectedCategory"), object: nil, queue: nil, using: block)
    }
    private func removeObserver() {
        if let observer = observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    @objc func clickedButton(sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectedCategory"), object: sender.titleLabel?.text)
    }
    private func commonInit() {

        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        scrollView.isPagingEnabled = true
        scrollView.isDirectionalLockEnabled = true
        scrollView.backgroundColor = UIColor.red
      //  self.backgroundColor = UIColor.clear
        self.addSubview(scrollView)

    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

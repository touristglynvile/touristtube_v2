//
//  NearByHotel.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 16/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import MapKit

class NearByHotel: NSObject, MKAnnotation {

    let title: String?
    let locationName: String?
    let coordinate: CLLocationCoordinate2D
    let price: String?
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D, price: String) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        self.price = price
        
        super.init()
    }
    var subtitle: String? {
        return price
    }
}

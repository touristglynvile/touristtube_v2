
//
//  NearByViewController.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 14/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit
import MapKit

class NearByViewController: UIViewController {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnFilters: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnHideInfo: UIButton!
    
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var collectionView: UICollectionView!
    var isInfoHidden = false
    func setupUI() {
        btnFilters.layer.cornerRadius = 18
        btnClose.layer.cornerRadius = btnClose.frame.size.width/2
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        

    }
    @IBAction func clickedClose(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickedHideInfo(_ sender: Any) {
        isInfoHidden = !isInfoHidden
        
        UIView.animate(withDuration: 0.5) {
            self.collectionView.isHidden = self.isInfoHidden
        }
        
        if isInfoHidden {
            btnHideInfo.setTitle("Show Info", for: .normal)
        }
        else {
            btnHideInfo.setTitle("Hide Info", for: .normal)
        }
    }
}

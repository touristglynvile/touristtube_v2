//
//  NearByMapView.swift
//  TTNearBy
//
//  Created by Glynvile Satago on 16/11/2018.
//  Copyright © 2018 TouristTube Philippines Inc. All rights reserved.
//

import UIKit
import MapKit

class NearByMapView: MKMapView, MKMapViewDelegate {

    var location: CLLocation?
    
    func setLocation() {
        let initialLocation = CLLocation(latitude: 10.308250, longitude: 123.912350)
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion(center: initialLocation.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        self.setRegion(coordinateRegion, animated: true)
    }
    func setAnnotations() {
        let locA = NearByHotel(title: "King David", locationName: "Waikiki 23 Park", coordinate: CLLocationCoordinate2D(latitude: 10.308250, longitude: 123.912350), price: "$4000")
        
        let locB = NearByHotel(title: "Master George", locationName: "Waikiki Park", coordinate: CLLocationCoordinate2D(latitude: 10.311790, longitude: 123.918053), price: "$500")
        
        let locC = NearByHotel(title: "Queen Cali", locationName: "Waikiki", coordinate: CLLocationCoordinate2D(latitude: 10.311780, longitude: 123.916410), price: "$6070")
        

        self.addAnnotations([locA, locB, locC])
       // self.reloadInputViews()
    }
    func commonInit() {
        self.setLocation()
        self.setAnnotations()
        
        self.delegate = self
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    //MARK: ~ MapView Delegate methods
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? NearByHotel else { return nil }
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.glyphText = annotation.price
            view.markerTintColor = ttGreen
            //view.glyphImage = UIImage(named: "map-pin", in: Bundle(for: type(of: self)), compatibleWith: nil)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
}
